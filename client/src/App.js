import React, { Component } from 'react';
import './App.css';
import {Container} from 'reactstrap';
import {ModalHeader,Button,Table,Modal,ModalBody,Input,Form,FormGroup,Label,Alert,ListGroupItem,ListGroup} from 'reactstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import { ChromePicker } from 'react-color';
import axios from 'axios';
var HexaColor = '#562222';
const ColorPicker = (props) => {
  return (
      <ChromePicker
          color='#562222'
        //  value={props.value}
          onChange={ color => {
            console.log(color.hex)
            HexaColor = color.hex
            console.log(HexaColor)
          }}
      />
  )
};


class App extends Component {

  state={
    modal:false,
    Websitename:'',
    Visitors:"",
    color:'',
    msg:'',
    PieChartData:[],
}
componentDidMount(){
  console.log(HexaColor)

  axios.get('api/piechart/getallpiechart')
  .then(res =>{
      console.log(res)
     this.setState({
      PieChartData:res.data 
     })
  }).catch( err =>{

  })
}


toggle = ()=>{
  this.setState({modal:!this.state.modal})
}
onChange = (e) =>{
  this.setState({[e.target.name]:e.target.value})
}
onSubmit = (e) =>{
 e.preventDefault();

 if(!this.state.Websitename || !this.state.Visitors || !HexaColor){
  this.setState({msg:'Please Enter all fields'});
  return true;
}
if(typeof this.state.Visitors !== 'number'){
this.setState({msg:'Visitors Count Should be a number'});
return true;
}
 const newItem ={
   //  id: uuid(),
     name: this.state.Websitename,
     value:this.state.Visitors,
     color:HexaColor
 };

 this.addItems(newItem);
 this.toggle();
}

AddItem = (e) =>{
  if(!this.state.Websitename || !this.state.Visitors || !HexaColor){
      this.setState({msg:'Please Enter all fields'});
      return true;
  }
  if(typeof this.state.Visitors !== 'number'){
    this.setState({msg:'Visitors Count Should be a number'});
    return true;
   }
  const newItem ={
    //  id: uuid(),
      name: this.state.Websitename,
      value:this.state.Visitors,
      color:HexaColor
  };

  this.addItems(newItem);
  this.toggle();
}

addItems(newItem){
  console.log(newItem)
}

  render(){
  return (
    <div className="App">
    <Container>
     
              <div className="modalClass" style={{marginTop: '3rem'}}>
               <Button onClick={()=>{
               this.toggle();
            }} color='dark' style={{marginBottom: '2rem'}}>
                Add Item
            </Button>
        

            <Modal isOpen={this.state.modal} toggle={this.toggle}>
              <ModalHeader toggle={this.toggle}>
                Add data to PieChart
              </ModalHeader>
              <ModalBody>
              {this.state.msg?<Alert color='danger'>{this.state.msg}</Alert>:null}
                 <Form onSubmit={this.onSubmit}>
                   <FormGroup>

                      <Label for="item">WebSite Name</Label>
                      <Input 
                         type="text" 
                         name="Websitename" 
                         id="Websitename" 
                         placeholder="Add shopping item"
                         onChange={this.onChange}
                       /> 

                        <Label style={{marginTop: '1rem'}} for="item">Daily Visitors</Label>
                      <Input 
                         type="text" 
                         name="Visitors" 
                         id="Visitors" 
                         placeholder="Add shopping item"
                         onChange={this.onChange}
                       /> 

                        <Label style={{marginTop: '1rem'}} for="item">color</Label>
                        {ColorPicker()}
                          <Button onClick={()=>{
               this.AddItem();
            }} color='dark' style={{marginTop: '2rem'}} block>
                Add Item
            </Button>
                   </FormGroup>
                 </Form>
              </ModalBody>
            </Modal>
            </div>

            <Table>
        <thead>
          <tr>
            <th>Website Name</th>
            <th>Visitors</th>
            <th>Color</th>
          </tr>
        </thead>
        <tbody>

        {this.state.PieChartData.map(({name,value,color,_id}) =>(
          <tr>
            <td>{name}</td>
            <td>{value}</td>
            <td> 
              <div style={{backgroundColor:'red'}}> {color}</div>
              </td>
              <td>
              <Button  className='remove-btn' color="danger" size="sm" onClick={()=>{
                       
                     /*   this.setState( state =>({
                            items: state.items.filter(item => item.id !== id)
                        }));  */
                    }} style={{marginRight:'0.5rem'}}> 
                        &times;
                    </Button>
              </td>
          </tr>
           ))}
        </tbody>
      </Table>


    </Container>
    </div>
  );
          }
}

export default App;
