var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
const piechart = require('./routes/api/PieChart')

var app=express();
app.use(bodyparser.json());

//db config
const db = "mongodb+srv://mern_shopping:mlab*password12@cluster0-fwb6z.mongodb.net/test?retryWrites=true";


mongoose.connect(db,{ useNewUrlParser: true,createIndexes:true})
      .then(() => {
          console.log("db connected successfully")
      })
      .catch(err => console.log(err));



//routes
app.use('/api/piechart',piechart);

const port = process.env.PORT || 5000;

app.listen(port,()=>{
    console.log(`server started on port ${port}`);
})