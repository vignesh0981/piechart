var express = require('express');
var router = express.Router();
const PiechartModel = require('../../models/PieChart');



router.post('/addpiechart',(req,res) => {
  //  res.send("adding piechart");
  const {name,value,color} = req.body;

  //validation
  if(!name || !value || !color){
      return res.status(400).json({msg:"please fill all fields"})
  }
    const newItem = new PiechartModel({
        name : req.body.name,
        value : req.body.value,
        color : req.body.color,
    });
    newItem.save().then(item => res.json(item))
    .catch( err =>{
        console.log(err);
    });
});

router.get('/getallpiechart',(req,res) => {
    PiechartModel.find()
    .then(items =>{
        res.json(items);
    })
  });

  router.get('/deletepie/:id',(req,res) => {
    PiechartModel.findById(req.params.id).then(
        item => item.remove().then(() => res.json({"success":true}))
    ).catch( err => res.status(404).json({"success":false}));
  });
module.exports = router;