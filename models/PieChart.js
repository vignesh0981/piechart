const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const PiechartSchema = new Schema({
name:{
    type:String,
    required:true
},
value:{
    type:String,
    required:true
},
color:{
    type:String,
    required:true
},
});


module.exports = PiechartModel = mongoose.model('piechart',PiechartSchema);
